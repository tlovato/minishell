# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/09 12:59:16 by tlovato           #+#    #+#              #
#    Updated: 2016/09/28 14:17:47 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell
INC_DIR = includes
SRCS_DIR = srcs/

SRCS_FILES = main.c command.c utils.c ft_echo.c ft_env.c ft_setenv.c\
ft_unsetenv.c  ft_cd.c ft_cd2.c linked_list.c ft_exit.c ft_env2.c command2.c\
whitespace.c

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

%.o: %.c
		gcc -g -o $@ -c $<

$(NAME): $(OBJS)
					@echo "\033[1;32m Objects done\033[m"
					@make -C libft/
					@gcc -o $@ $^ -L libft/ -lft -g3
					@echo "\033[1;32m Minishell done\033[m"

clean:
					@rm -f $(OBJS)
					@echo "\033[1;31m Cleaning done\033[m"

fclean: clean
					@rm -f $(NAME)
					@cd libft && make fclean
					@echo "\033[1;31m Fcleaning done\033[m"

re: fclean all

.PHONY: all clean fclean re
