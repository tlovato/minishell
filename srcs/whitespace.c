/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whitespace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 19:30:43 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/26 19:30:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static int			calc_whitespace_len(char *str)
{
	int				i;
	int				j;
	int				space;

	i = ft_strlen(str);
	j = 0;
	space = 0;
	while (str[j])
	{
		if (str[j] == ' ' || str[j] == '\t')
		{
			space++;
			i--;
		}
		j++;
	}
	return (i + space);
}

static char			*do_boucle(char *ret, char *tmp)
{
	int				i;
	int				j;

	i = 0;
	j = 0;
	while (tmp[i])
	{
		if (tmp[i] == ' ' || tmp[i] == '\t')
		{
			while (tmp[i] && (tmp[i] == ' ' || tmp[i] == '\t'))
				i++;
			if (j > 0)
			{
				ret[j] = ' ';
				j++;
			}
		}
		ret[j] = tmp[i];
		i++;
		j++;
	}
	ret[j] = '\0';
	return (ret);
}

char				*whitespace(char *tmp)
{
	int				len;
	char			*ret;

	len = calc_whitespace_len(tmp);
	ret = (char *)malloc(sizeof(char) * (len + 1));
	ret = do_boucle(ret, tmp);
	free(tmp);
	return (ret);
}
