/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 18:12:57 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/16 16:25:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static void			var_env(char *line, int len, t_env *menv, int i)
{
	char			*sub;
	t_env			*tmp;
	char			*tmpline;

	tmp = menv;
	while (tmp)
	{
		sub = ft_strsub(tmp->line, 0, ft_strlen(line));
		tmpline = ft_strjoin(line, "=");
		if (!ft_strncmp(sub, &tmpline[1], ft_strlen(tmpline)))
			ft_putstr(&tmp->line[ft_strlen(sub)]);
		free(sub);
		free(tmpline);
		tmp = tmp->next;
	}
}

static void			display(char *line)
{
	int				i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '\"' && line[i + 1] == '\0')
			line[i] = '\0';
		else if (line[i] == '\"')
			i++;
		ft_putchar(line[i]);
		i++;
	}
}

int					ft_echo(char **tab, t_env *menv)
{
	int				i;
	int				len;

	i = 1;
	len = ft_strtablen(tab);
	while (tab[i])
	{
		if (tab[i][0] == '$')
			var_env(tab[i], len, menv, i);
		else
			display(tab[i]);
		if (tab[i + 1] != NULL)
			ft_putchar(' ');
		i++;
	}
	ft_putchar('\n');
	return (0);
}
