/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 18:35:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/26 18:35:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

int					is_exe(char **mshav, char **paths, char **pthcmd)
{
	int				i;

	i = 1;
	while (mshav[i])
	{
		find_cmd(paths, mshav[i], pthcmd);
		if (*pthcmd)
			return (1);
		i++;
	}
	return (0);
}

int					verif_var(char *arg, struct s_env *menv)
{
	char			*sub;
	struct s_env	*tmp;

	tmp = menv;
	sub = ft_strsub(arg, 0, find_equal(arg) + 1);
	while (tmp)
	{
		if (ft_strstr(tmp->line, sub))
		{
			free(sub);
			return (1);
		}
		tmp = tmp->next;
	}
	if (sub)
		free(sub);
	return (0);
}
