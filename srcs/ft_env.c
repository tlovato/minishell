/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 20:00:47 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/11 20:00:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static int			find_ind(char **mshav)
{
	int				i;

	i = (!ft_strcmp(mshav[1], "-i")) ? 2 : 1;
	while (mshav[i])
	{
		if (!ft_strrchr(mshav[i], '='))
			return (i);
		i++;
	}
	return (i);
}

static void			do_exe(char *pthcmd, char **mshav, char **env)
{
	pid_t			pid;
	int				i;

	i = 0;
	pid = fork();
	if (pid == 0)
	{
		while (mshav[i])
		{
			if (ft_strrchr(mshav[i], '='))
				execve(pthcmd, &mshav[find_ind(mshav)], &mshav[i]);
			if (!ft_strcmp(mshav[1], "-i"))
				execve(pthcmd, &mshav[find_ind(mshav)], NULL);
			else if (mshav[i + 1] == NULL)
				execve(pthcmd, &mshav[find_ind(mshav)], env);
			i++;
		}
	}
	else
		wait(NULL);
}

static void			put_var_value(struct s_env *env, char *arg)
{
	struct s_env	*tmp;
	char			*sub;
	int				i;

	tmp = env;
	i = find_equal(arg);
	sub = ft_strsub(arg, 0, i);
	while (tmp)
	{
		if (ft_strstr(tmp->line, sub))
		{
			ft_putstr(tmp->line);
			ft_putendl(&arg[i + 1]);
		}
		else if (!ft_strstr(tmp->line, sub))
			ft_putendl(tmp->line);
		tmp = tmp->next;
	}
	free(sub);
}

static int			condition(int len, char *arg, t_env *menv)
{
	return (len == 1 || (ft_strcmp(arg, "-i") && !verif_var(arg, menv)));
}

void				ft_env(char **mshav, struct s_denv **env, char **paths)
{
	int				len;
	int				i;
	char			*pthcmd;

	i = 1;
	pthcmd = NULL;
	len = ft_strtablen(mshav);
	if (is_exe(mshav, paths, &pthcmd))
		do_exe(pthcmd, mshav, (*env)->nenv);
	else
	{
		if (condition(len, mshav[1], (*env)->menv))
			display_env((*env)->menv);
		while (mshav[i])
		{
			if (ft_strrchr(mshav[i], '='))
			{
				if (verif_var(mshav[i], (*env)->menv))
					put_var_value((*env)->menv, mshav[i]);
				else if (!verif_var(mshav[i], (*env)->menv))
					ft_putendl(mshav[i]);
			}
			i++;
		}
	}
}
