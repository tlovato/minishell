/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 12:00:29 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/12 12:00:41 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static void			add_var(char **mshav, t_env **menv)
{
	t_env			*new;
	t_env			*tmp;

	tmp = *menv;
	new = (struct s_env *)malloc(sizeof(struct s_env));
	new->line = ft_strdup(mshav[1]);
	new->next = NULL;
	if (tmp)
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*menv = new;
}

void				ft_setenv(char **mshav, struct s_denv *env)
{
	char			*sub;

	if (ft_strtablen(mshav) == 1)
		display_env(env->menv);
	else if (ft_strtablen(mshav) == 2 && find_equal(mshav[1]))
	{
		sub = ft_strsub(mshav[1], 0, find_equal(mshav[1]));
		if (find_vars(env->menv, sub) &&
			!ft_strncmp(find_vars(env->menv, sub), sub, ft_strlen(sub)))
			change_vars(env->menv, sub, &mshav[1][ft_strlen(sub)]);
		else
			add_var(mshav, &env->menv);
		free(sub);
	}
	else if (ft_strtablen(mshav) > 2)
		ft_printf("setenv: Too many arguments.\n");
}
