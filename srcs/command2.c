/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 18:36:25 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/26 20:40:04 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

void				find_cmd(char **paths, char *cmd, char **pthcmd)
{
	int				i;
	DIR				*dirp;
	struct dirent	*dp;

	i = 0;
	while (paths[i])
	{
		if ((dirp = opendir(paths[i])) != NULL)
		{
			while ((dp = readdir(dirp)) != NULL)
				if (!ft_strcmp(dp->d_name, cmd))
				{
					paths[i] = ft_strjoinfree(paths[i], "/", 1);
					paths[i] = ft_strjoinfree(paths[i], cmd, 1);
					if (!access(paths[i], X_OK))
						*pthcmd = paths[i];
				}
			closedir(dirp);
		}
		i++;
	}
}

void				cnf(char *cmd)
{
	ft_printf("minishell: command not found: ");
	ft_putendl(cmd);
}

static int			calc_len(t_env *menv)
{
	t_env			*tmp;
	int				ret;

	tmp = menv;
	ret = 0;
	while (tmp)
	{
		tmp = tmp->next;
		ret++;
	}
	return (ret);
}

char				**change_env(t_env *menv)
{
	char			**ret;
	t_env			*tmp;
	int				i;

	i = 0;
	tmp = menv;
	ret = (char **)malloc(sizeof(char *) * (calc_len(tmp) + 1));
	while (tmp)
	{
		ret[i] = ft_strnew(ft_strlen(tmp->line));
		ft_strcpy(ret[i], tmp->line);
		tmp = tmp->next;
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
