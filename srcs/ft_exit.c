/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 14:41:36 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/26 14:41:39 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

void		ft_exit(char **cmd)
{
	if (!cmd[1])
		exit(0);
	else if (cmd[1] && !cmd[2])
		exit(ft_atoi(cmd[1]));
	else
		ft_printf("exit: too many arguments\n");
}
