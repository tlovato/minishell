/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 10:54:34 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/16 15:00:31 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static void			pars_cmd(char *c, char **mav, struct s_denv **env, char **p)
{
	char			*tmp;

	tmp = NULL;
	if (mav[1] && mav[1][0] == '"' && mav[1][ft_strlen(mav[1]) - 1] == '"')
	{
		tmp = ft_strnew(ft_strlen(mav[1]) - 2);
		ft_strncpy(tmp, &mav[1][1], ft_strlen(mav[1]) - 2);
		free(mav[1]);
		mav[1] = tmp;
	}
	if (!ft_strcmp(c, "echo"))
		ft_echo(mav, (*env)->menv);
	if (!ft_strcmp(c, "env"))
		ft_env(mav, env, p);
	if (!ft_strcmp(c, "unsetenv"))
		ft_unsetenv(mav, *env);
	if (!ft_strcmp(c, "setenv"))
		ft_setenv(mav, *env);
	if (!ft_strcmp(c, "cd"))
		ft_cd(mav, *env);
}

static void			fork_one(char *pthcmd, char **mav, char **env)
{
	pid_t			pid;
	char			*tmp;

	tmp = NULL;
	pid = fork();
	if (mav[1] && mav[1][0] == '"' && mav[1][ft_strlen(mav[1]) - 1] == '"')
	{
		tmp = ft_strnew(ft_strlen(mav[1]) - 2);
		ft_strncpy(tmp, &mav[1][1], ft_strlen(mav[1]) - 2);
		free(mav[1]);
		mav[1] = tmp;
	}
	if (pid == 0)
		execve(pthcmd, mav, env);
	else
		wait(NULL);
}

static void			fork_two(char **mav, char **env)
{
	pid_t			pid;

	pid = fork();
	if (pid == 0)
		execve(mav[0], mav, env);
	else
		wait(NULL);
}

static void			do_stuff(char *pth, char *cmd, char **mav, t_denv **e)
{
	char			**pths;
	char			*pthcmd;
	int				cond;

	pthcmd = NULL;
	pths = NULL;
	cond = (ft_strcmp(cmd, "echo") && ft_strcmp(cmd, "env")
		&& ft_strcmp(cmd, "unsetenv") && ft_strcmp(cmd, "setenv")
		&& ft_strcmp(cmd, "cd"));
	if (pth)
	{
		pths = ft_strsplit(pth, ':');
		find_cmd(pths, cmd, &pthcmd);
	}
	if (pthcmd != NULL && cond)
		fork_one(pthcmd, mav, (*e)->env);
	else if (!pthcmd && cond && mav[0][0] == '/')
		fork_two(mav, (*e)->nenv);
	else if (!cond)
		pars_cmd(cmd, mav, e, pths);
	else
		cnf(cmd);
	if (pths)
		ft_free_str_tab(pths);
}

void				do_cmd(char *pth, char *cmd, char **mav, struct s_denv **e)
{
	if (!cmd || cmd[0] == '\0')
		return ;
	if (!pth && cmd[0] != '/' && (ft_strcmp(cmd, "setenv") &&
		ft_strcmp(cmd, "unsetenv") && ft_strcmp(cmd, "echo")
		&& ft_strcmp(cmd, "exit") && ft_strcmp(cmd, "cd")
		&& ft_strcmp(cmd, "env")))
		cnf(cmd);
	else
		do_stuff(pth, cmd, mav, e);
}
