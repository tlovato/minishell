/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 09:53:31 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/13 09:53:41 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static void			erase_var(t_env **menv, char *line, int len)
{
	t_env			*cur;
	t_env			*tmp;

	cur = *menv;
	tmp = *menv;
	while (tmp && ft_strncmp(tmp->line, line, len))
	{
		cur = tmp;
		tmp = tmp->next;
	}
	if (cur == tmp)
		*menv = (*menv)->next;
	else if (tmp)
		cur->next = tmp->next;
	free(tmp->line);
	free(tmp);
	tmp = NULL;
}

static void			free_subs(char *l1, char *l2)
{
	free(l1);
	free(l2);
}

void				ft_unsetenv(char **mav, struct s_denv *env)
{
	struct s_env	*tmp;
	int				i;
	char			*tmpline;
	char			*sub;

	tmp = env->menv;
	if (ft_strtablen(mav) == 1)
		ft_putendl("unsetenv: Too few arguments.");
	else
	{
		tmpline = ft_strjoin(mav[1], "=");
		while (tmp)
		{
			sub = ft_strsub(tmp->line, 0, ft_strlen(tmpline));
			if (!ft_strcmp(sub, tmpline))
			{
				erase_var(&env->menv, tmp->line, ft_strlen(tmpline));
				free_subs(tmpline, sub);
				return ;
			}
			free(sub);
			tmp = tmp->next;
		}
	}
	free(tmpline);
}
