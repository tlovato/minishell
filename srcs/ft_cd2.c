/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 06:23:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/15 06:23:45 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

char		*find_vars(t_env *menv, char *str)
{
	int		i;
	char	*pwd;

	i = 0;
	while (menv)
	{
		pwd = ft_strstr(menv->line, str);
		if (pwd != NULL)
			return (menv->line);
		menv = menv->next;
	}
	return (NULL);
}

void		change_vars(t_env *menv, char *find, char *change)
{
	t_env	*tmp;
	char	*sub;
	int		len;

	tmp = menv;
	len = ft_strlen(find);
	while (tmp)
	{
		sub = ft_strsub(tmp->line, 0, ft_strlen(find));
		if (!ft_strcmp(sub, find))
		{
			ft_bzero(&tmp->line[len], ft_strlen(tmp->line) - len);
			tmp->line = ft_strjoinfree(tmp->line, change, 1);
		}
		free(sub);
		tmp = tmp->next;
	}
}

void		cd_errorpath(char *path, char *fpwd, char *fopwd, char *fpath)
{
	if (fpwd)
		free(fpwd);
	if (fopwd)
		free(fopwd);
	if (fpath)
		free(fpath);
	ft_printf("cd: no such file or diectory: ");
	ft_putendl(path);
}

void		cd_error(char *arg)
{
	ft_printf("cd: string not in pwd: ");
	ft_putendl(arg);
}

void		go_arg(char **mav)
{
	if (chdir(mav[1]) < 0)
	{
		ft_printf("cd: no such file or directory: ");
		ft_putendl(mav[1]);
	}
}
