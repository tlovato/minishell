/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 13:33:38 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/03 11:45:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

static void		case_one(t_env *menv, char *pth)
{
	char		*pwd;
	char		*path;
	char		*npath;
	char		buf[100];

	if (find_vars(menv, "PWD="))
	{
		pwd = ft_strdup(find_vars(menv, "PWD="));
		path = "/nfs/2015/t/tlovato";
		npath = !pth ? path : ft_strjoin(path, &pth[1]);
		if (chdir(npath) < 0)
		{
			cd_errorpath(npath, pwd, NULL, path ? npath : NULL);
			return ;
		}
		change_vars(menv, "OLDPWD=", &pwd[4]);
		change_vars(menv, "PWD=", getcwd(buf, 100));
		free(pwd);
		if (pth)
			free(npath);
	}
	else
		ft_printf("cd: No home directory\n");
}

static void		case_two(t_env *menv)
{
	char		*old_pwd;
	char		*pwd;

	if (find_vars(menv, "OLDPWD"))
	{
		old_pwd = ft_strdup(find_vars(menv, "OLDPWD="));
		pwd = ft_strdup(find_vars(menv, "PWD="));
		if (chdir(&old_pwd[7]) < 0)
		{
			cd_errorpath(&old_pwd[7], pwd, old_pwd, NULL);
			return ;
		}
		change_vars(menv, "PWD=", &old_pwd[7]);
		change_vars(menv, "OLDPWD=", &pwd[4]);
		free(pwd);
		free(old_pwd);
	}
	else
		ft_printf(": No such file or directory\n");
}

static void		case_three(t_env *menv, char *path)
{
	char		*pwd;
	char		buf[100];

	if (find_vars(menv, "PWD="))
		pwd = ft_strdup(find_vars(menv, "PWD="));
	if (chdir(path) < 0)
	{
		cd_errorpath(path, pwd, NULL, NULL);
		return ;
	}
	if (find_vars(menv, "PWD="))
	{
		change_vars(menv, "OLDPWD=", &pwd[4]);
		change_vars(menv, "PWD=", getcwd(buf, 100));
		free(pwd);
	}
}

void			ft_cd(char **mav, struct s_denv *envs)
{
	int			len;

	len = ft_strtablen(mav);
	if (len > 2)
		cd_error(mav[1]);
	else
	{
		if (len == 1)
			case_one(envs->menv, NULL);
		else if (len == 2 && ft_strstr(mav[1], "~"))
			case_one(envs->menv, mav[1]);
		else if (!ft_strcmp(mav[1], "-"))
			case_two(envs->menv);
		else
			case_three(envs->menv, mav[1]);
	}
}
