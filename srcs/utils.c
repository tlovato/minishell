/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 17:10:33 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/11 17:10:46 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

int					cmd_cmp(char *cmd, char *str)
{
	int				i;
	int				j;
	int				cond;

	i = 0;
	j = 0;
	while (cmd[i] == ' ' || cmd[i] == '\t')
		i++;
	while ((cmd[i] != ' ' || cmd[i] != '\t') &&
		(cmd[i] == str[j]) && cmd[i] && str[j])
	{
		i++;
		j++;
	}
	cond = cmd[i] == ' ' || cmd[i] == '\t';
	return (cmd[cond ? i - 1 : i] - str[cond ? j - 1 : j]);
}

char				**cpy_env(char **env)
{
	int				len;
	char			**ret;
	int				i;

	i = 0;
	len = ft_strtablen(env);
	ret = (char **)malloc(sizeof(char *) * (len + 1));
	while (i < len)
	{
		ret[i] = (char *)malloc(sizeof(char) * (ft_strlen(env[i]) + 1));
		ft_strcpy(ret[i], env[i]);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}

int					find_equal(char *arg)
{
	int				i;

	i = 0;
	while (arg[i] != '=' && arg[i])
		i++;
	if (arg[i] == '\0')
		i = 0;
	return (i);
}
