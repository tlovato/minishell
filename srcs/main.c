/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 12:15:34 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/10 12:15:36 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

char				*get_path(t_env *menv)
{
	int				i;
	char			*sub;
	t_env			*tmp;

	i = 0;
	tmp = menv;
	while (tmp)
	{
		i = find_equal(tmp->line) + 1;
		sub = ft_strsub(tmp->line, 0, i);
		if (!ft_strcmp(sub, "PATH="))
		{
			free(sub);
			return (&tmp->line[5]);
		}
		if (sub)
			free(sub);
		tmp = tmp->next;
	}
	return (NULL);
}

static char			*get_line(void)
{
	char			buf;
	char			*newstock;
	char			*cat;
	char			*ret;

	newstock = ft_strnew(1);
	ft_bzero(newstock, 1);
	while (read(0, &buf, 1) && buf != '\n')
	{
		cat = ft_strnew(1);
		cat[0] = buf;
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}

static char			**init_minishell(void)
{
	char			*tmp;
	char			**cmd;

	ft_printf("$> ");
	tmp = get_line();
	tmp = whitespace(tmp);
	cmd = ft_strsplit(tmp, ' ');
	if (ft_strtablen(cmd) == 1)
	{
		ft_free_str_tab(cmd);
		cmd = ft_strsplit(tmp, '\t');
	}
	free(tmp);
	return (cmd);
}

static void			free_allocs(t_denv *envs)
{
	free_lst(envs->menv);
	ft_free_str_tab(envs->env);
	free(envs);
}

int					main(int ac, char **av, char **env)
{
	char			**cmd;
	int				exe;
	char			*pth;
	struct s_denv	*envs;

	exe = 1;
	envs = (t_denv *)malloc(sizeof(t_denv));
	envs->menv = get_env_lst(env, 0);
	envs->env = cpy_env(env);
	while (exe)
	{
		cmd = init_minishell();
		if (cmd[0] && !cmd_cmp(cmd[0], "exit"))
			ft_exit(cmd);
		else
		{
			pth = get_path(envs->menv);
			envs->nenv = change_env(envs->menv);
			do_cmd(pth, cmd[0], cmd, &envs);
			ft_free_str_tab(envs->nenv);
		}
		ft_free_str_tab(cmd);
	}
	free_allocs(envs);
	return (0);
}
