/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linked_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 13:33:19 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/22 17:26:12 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/minishell.h"

t_env				*get_env_lst(char **env, int i)
{
	t_env			*new;

	if (i < ft_strtablen(env))
	{
		new = (struct s_env *)malloc(sizeof(struct s_env));
		new->line = ft_strdup(env[i]);
		i += 1;
		new->next = get_env_lst(env, i);
		return (new);
	}
	return (NULL);
}

void				free_lst(struct s_env *env)
{
	struct s_env	*tmp;

	while (env)
	{
		tmp = env->next;
		free(env->line);
		free(env);
		env = tmp;
	}
	free(env);
}

void				display_env(struct s_env *env)
{
	while (env)
	{
		ft_putendl(env->line);
		env = env->next;
	}
}
