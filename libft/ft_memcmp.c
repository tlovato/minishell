/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:37:44 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:29:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *str, const void *str2, size_t n)
{
	unsigned char		*s;
	unsigned char		*s2;

	s = (unsigned char *)str;
	s2 = (unsigned char *)str2;
	if (s == s2)
		return (0);
	while (n--)
	{
		if (*s != *s2)
			return (*s - *s2);
		s++;
		s2++;
	}
	return (0);
}
