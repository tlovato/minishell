/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 09:01:43 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:33:35 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str, const char *str2, size_t n)
{
	size_t		i;
	size_t		j;
	size_t		k;

	i = 0;
	k = 0;
	if (str2[i] == '\0')
		return ((char *)&str[i]);
	while (str[i])
	{
		j = i;
		k = 0;
		while ((str[j] == str2[k]) && (j < n))
		{
			j++;
			k++;
			if (str2[k] == '\0')
				return ((char *)&str[i]);
		}
		i++;
	}
	return (NULL);
}
