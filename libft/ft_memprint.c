/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memprint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 15:18:40 by tlovato           #+#    #+#             */
/*   Updated: 2016/06/01 15:18:42 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_memprint(const char *str, int len)
{
	int		i;

	i = 0;
	while (i < len)
	{
		ft_putchar(str[i]);
		i++;
	}
}
