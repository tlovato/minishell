/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 12:16:16 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/10 12:16:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include "libftprintf.h"
# include <signal.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <dirent.h>

typedef struct		s_env
{
	char			*line;
	struct s_env	*next;
}					t_env;

typedef struct		s_denv
{
	struct s_env	*menv;
	char			**env;
	char			**nenv;
}					t_denv;

char				*get_path(t_env *menv);
void				do_cmd(char *p, char *cmd, char **av, struct s_denv **envs);
void				find_cmd(char **paths, char *cmd, char **pthcmd);
void				cnf(char *cmd);
int					cmd_cmp(char *cmd, char *str);
char				**cpy_env(char **env);
t_env				*get_env_lst(char **env, int i);
void				free_lst(struct s_env *env);
void				display_env(struct s_env *env);
int					is_exe(char **mshav, char **paths, char **pthcmd);
int					find_equal(char *str);
int					verif_var(char *arg, struct s_env *menv);
int					ft_echo(char **tab, t_env *menv);
void				ft_env(char **mshav, struct s_denv **env, char **paths);
void				ft_setenv(char **mshav, struct s_denv *env);
void				ft_unsetenv(char **mav, struct s_denv *env);
void				ft_cd(char **mav, struct s_denv *envs);
void				go_arg(char **mav);
void				cd_error(char *arg);
void				change_vars(t_env *menv, char *find, char *change);
char				*find_vars(t_env *menv, char *str);
void				cd_errorpath(char *p, char *fpwd, char *fopwd, char *fpath);
char				**change_env(t_env *menv);
void				erase_spaces(char **tab);
void				ft_exit(char **cmd);
char				*whitespace(char *tmp);

#endif
